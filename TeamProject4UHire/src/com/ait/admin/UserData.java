package com.ait.admin;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.annotation.PostConstruct;
@ManagedBean
@SessionScoped

public class UserData {

	private ArrayList<User> users;
	@PostConstruct
	public void init() {
		users=new ArrayList<User>();
		User firstUser = new User("root", "superadmin");
		users.add(firstUser);
	}

	public ArrayList<User> getUsers() {
		return users;
	}
}