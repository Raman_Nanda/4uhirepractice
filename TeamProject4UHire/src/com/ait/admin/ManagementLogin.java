package com.ait.admin;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ManagementLogin implements Serializable{
	
	
	//Declare instance variables
		private static final long serialVersionUID = 1L;
		private ArrayList<CreateManagementStaff> managementStaffs;
		private String username;
		private String password;

		
		//instantiate an object of management staff
		public void init(){
			managementStaffs = new ArrayList<CreateManagementStaff>();
			this.username = "";
			this.password = "";				
		}
		//Return username
		public String getUsername() {
			return username;
		}
		//Set username
		public void setUsername(String email) {
			this.username = email;
		}
		//Get password
		public String getPassword() {
			return password;
		}
		//Set password
		public void setPassword(String password) {
			this.password = password;
		}
		
		//Check if management staff username exist
		public boolean isAuthenticateUsername(String username){
			boolean found = false;
			for(CreateManagementStaff staff: managementStaffs){
				found = (staff.getUsername().equalsIgnoreCase(username))? true:false;
				setUsername(username);
			}
			return found;
		}
		
		//Check if management staff password exists
		public boolean isAuthenticatePassword(String password){
			boolean found = false;
			for(CreateManagementStaff staff: managementStaffs){
				found = (staff.getPassword().equals(password))? true:false;	
				setPassword(password);
			}
			return found;
		}
		
		//Return true if management staff is authenticated
		public String isChecked(boolean username, boolean password){
			String status;
			status = (username == true && password == true)? String.format("Welcome %s",getUsername()):String.format("Authentication failed");		
			return status;			
		}
}
