package com.ait.admin;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ManagementStaffMethods {
	private String incorrectEntry;
	private String inputFirstName;
	private String inputLastName;
	private String inputPassword;
	private String inputUsername;
	private String inputDateOfBirth;
	private String inputAddress;
	private String inputGender;
	private String wrongUsername = "";
	
	private ArrayList<ManagementStaff> mgmtStaff;
	
	@PostConstruct
	public void init(){
		mgmtStaff=new ArrayList<ManagementStaff>();
		
	}
	

	public String getInputDateOfBirth() {
		return inputDateOfBirth;
	}


	public void setInputDateOfBirth(String inputDateOfBirth) {
		this.inputDateOfBirth = inputDateOfBirth;
	}


	public String getInputAddress() {
		return inputAddress;
	}


	public void setInputAddress(String inputAddress) {
		this.inputAddress = inputAddress;
	}


	public String getInputGender() {
		return inputGender;
	}


	public void setInputGender(String inputGender) {
		this.inputGender = inputGender;
	}


	public String getIncorrectEntry() {
		return incorrectEntry;
	}

	public void setIncorrectEntry(String incorrectEntry) {
		this.incorrectEntry = incorrectEntry;
	}

	public String getInputFirstName() {
		return inputFirstName;
	}

	public void setInputFirstName(String inputFirstName) {
		this.inputFirstName = inputFirstName;
	}

	public String getInputLastName() {
		return inputLastName;
	}

	public void setInputLastName(String inputLastName) {
		this.inputLastName = inputLastName;
	}

	public String getInputPassword() {
		return inputPassword;
	}

	public void setInputPassword(String inputPassword) {
		this.inputPassword = inputPassword;
	}

	public String getInputUsername() {
		return inputUsername;
	}

	public void setInputUsername(String inputUsername) {
		this.inputUsername = inputUsername;
	}


	public String getWrongUsername() {
		return wrongUsername;
	}

	public void setWrongUsername(String wrongUsername) {
		this.wrongUsername = wrongUsername;
	}

	public ArrayList<ManagementStaff> getMgmtStaff() {
		return mgmtStaff;
	}
	public String addManagementStaff(){
		boolean alreadyExist=false;
		for(ManagementStaff ms:mgmtStaff){
			if(ms.getUsername().equals(inputUsername)){
				alreadyExist=true;
				break;
			}
		}
		if(alreadyExist){
			wrongUsername="This Username is already taken";
		}else{
			ManagementStaff mgmt=new ManagementStaff(this.inputFirstName,this.inputLastName,this.inputGender,this.inputUsername,this.inputAddress,this.inputDateOfBirth,this.inputPassword);
			mgmtStaff.add(mgmt);
			inputFirstName = null;
			inputLastName = null;
			inputUsername = null;
			inputPassword = null;
			inputAddress=null;
			inputGender=null;
			inputDateOfBirth=null;
			wrongUsername = "";
			return "show-registration.jsf";
		}
		return null;
	}


	
	

}
