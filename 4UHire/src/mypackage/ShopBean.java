package mypackage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ShopBean {
	private String productId;
	private int quantity;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String addHandler(){
		CartBean cart= Helper.getBean("cartBean", CartBean.class);
		cart.addItemToCart(productId, quantity);
		return null;
		
	}
	public String removeHandler(){
		CartBean cart=Helper.getBean("cartBean", CartBean.class);
		cart.removeItemFromCart(productId);
		return null;
	}
	
	

}
