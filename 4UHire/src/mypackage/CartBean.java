package mypackage;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class CartBean {
	private String name;
	private String address;

	private ArrayList<Item> item=new ArrayList<Item>(); 

	public ArrayList<Item> getItem() {
		return item;
	}

	public void setItem(ArrayList<Item> item) {
		this.item = item;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void addItemToCart(String productId, int quantity){
		Item one=new Item(productId, quantity);
		item.add(one);

	}
	public void removeItemFromCart(String productId){
		for(Item items:item){
			if(items.getProductId().equals(productId)){
				item.remove(items);
				break;
				

			}
		}

	}
	public int getItemCount(){
		return item.size();
	}





}
